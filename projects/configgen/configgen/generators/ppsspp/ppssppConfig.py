#!/usr/bin/env python
import configgen.recalboxFiles as recalboxFiles
from configgen.Emulator import Emulator

def writePPSSPPConfig(system: Emulator):

    from configgen.settings.iniSettings import IniSettings
    settings = IniSettings(recalboxFiles.ppssppConfig, True)
    settings.loadFile(True)
    # Get Video driver
    from configgen.utils.videoMode import GetHasVulkanSupport
    videoDriver = "3 (VULKAN)" if GetHasVulkanSupport() == '1' else "0 (OPENGL)"
    # Display FPS
    settings.setInt("CPU", 'ShowFPSCounter', 3 if  system.ShowFPS else 0) # 1 for Speed%, 2 for FPS, 3 for both
    # Set video renderer OpenGl / vulkan
    settings.setString("Graphics", 'GraphicsBackend', videoDriver)
    settings.saveFile()
