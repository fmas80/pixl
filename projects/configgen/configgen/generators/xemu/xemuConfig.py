#!/usr/bin/env python
import configgen.recalboxFiles as recalboxFiles
from configgen.settings.iniSettings import IniSettings
from configgen.controllers.controller import ControllerPerPlayer
from configgen.settings.keyValueSettings import keyValueSettings

def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(txt)
    return 0

def GetLanguage() -> str:
    conf = keyValueSettings(recalboxFiles.recalboxConf)
    conf.loadFile(True)
    # Try to obtain from system language, then fallback to en
    Language = conf.getString("system.language", "en")[0:2].lower()
    return Language
        
def createXemuConfig(self, system, args, playersControllers: ControllerPerPlayer):

    gameRatio = self.GAME_RATIO[system.Ratio] if system.Ratio in self.GAME_RATIO else "scale"

    # Load Configuration
    xemuSettings = IniSettings(recalboxFiles.xemuIni, True)
    xemuSettings.loadFile(True)

    # Get Languages for xbox only
    language = GetLanguage()
    Log("language:{}".format(language));
    # set languageCode variable
    if language in self.XBOX_LANGUAGES:
        languageCode = language
    else:
        languageCode ="en" #en

    # System.files
    # Change bios for Sega Chihiro and upgrade mem_limit to 128
    if system.Name == "chihiro":
        xemuSettings.setString(self.SECTION_SYS_FILES, "flashrom_path", "'/recalbox/share/bios/chihiro/iND-BiOS.5003.67.bin'")
        xemuSettings.setString(self.SECTION_SYS_FILES, "bootrom_path", "'/recalbox/share/bios/chihiro/mcpx_1.0.bin'")
        xemuSettings.setString(self.SECTION_SYS_FILES, "hdd_path", "'/recalbox/share/saves/chihiro/xbox_hdd.qcow2'")
        xemuSettings.setString(self.SECTION_SYS_FILES, "eeprom_path", "'/recalbox/share/saves/chihiro/eeprom.bin'")
        xemuSettings.setString(self.SECTION_SYS, "mem_limit", "'128'") 
        xemuSettings.setString(self.SECTION_GENERAL, "skip_boot_anim", "true")
    else:
        xemuSettings.setString(self.SECTION_SYS_FILES, "flashrom_path", "'/recalbox/share/bios/xbox/Complex_4627.bin'")
        xemuSettings.setString(self.SECTION_SYS_FILES, "bootrom_path", "'/recalbox/share/bios/xbox/mcpx_1.0.bin'")
        xemuSettings.setString(self.SECTION_SYS_FILES, "hdd_path", "'/recalbox/share/saves/xbox/xbox_hdd.qcow2'")
        xemuSettings.setString(self.SECTION_SYS_FILES, "eeprom_path", "'/recalbox/share/system/configs/xemu/eeprom_{}.bin'".format(languageCode))
        xemuSettings.setString(self.SECTION_SYS, "mem_limit", "'64'")
        xemuSettings.setString(self.SECTION_GENERAL, "skip_boot_anim", "false")
        
    xemuSettings.setString(self.SECTION_GENERAL, "show_welcome", "false")
    # xemuSettings.setString(self.SECTION_GENERAL, "skip_boot_anim", "false")
    xemuSettings.setString(self.SECTION_GENERAL, "screenshot_dir", "'/recalbox/share/screenshots'")
    # perf
    xemuSettings.setString(self.SECTION_PERF, "hard_fpu", "true")
    # force on false for moment filled sdb2 partition on xemu v0.7.70
    xemuSettings.setString(self.SECTION_PERF, "cache_shaders", "false")
    # Audio 
    xemuSettings.setString(self.SECTION_AUDIO, "use_dsp", "false")
    # Display.ui
    xemuSettings.setString(self.SECTION_DISPLAY_UI, "fit", gameRatio)
    xemuSettings.setString(self.SECTION_DISPLAY_UI, "show_menubar", "false")
    # Display window
    xemuSettings.setString(self.SECTION_DISPLAY_WINDOW, "fullscreen_on_startup", "true")
    xemuSettings.setString(self.SECTION_DISPLAY_WINDOW, "vsync", "true")
    # Net
    xemuSettings.setString(self.SECTION_NET, "enable", "false")
    # Misc
    # xemuSettings.setString(self.SECTION_MISC, "user_token", "")
    # Inputs
    if 1 in playersControllers: xemuSettings.setString(self.SECTION_INPUT_BINDINGS, "port1", "'" + playersControllers[1].GUID + "'")
    if 2 in playersControllers: xemuSettings.setString(self.SECTION_INPUT_BINDINGS, "port2", "'" + playersControllers[2].GUID + "'")
    if 3 in playersControllers: xemuSettings.setString(self.SECTION_INPUT_BINDINGS, "port3", "'" + playersControllers[3].GUID + "'")
    if 4 in playersControllers: xemuSettings.setString(self.SECTION_INPUT_BINDINGS, "port4", "'" + playersControllers[4].GUID + "'")

    # Save configuration
    xemuSettings.saveFile()