################################################################################
#
# SLANG_SHADERS
#
################################################################################

LIBRETRO_SLANG_SHADERS_VERSION = ec7ca0125da2654f9f740c9a40e5bd0a944bfa35
LIBRETRO_SLANG_SHADERS_SITE = $(call github,libretro,slang-shaders,$(LIBRETRO_SLANG_SHADERS_VERSION))
LIBRETRO_SLANG_SHADERS_LICENSE = MIT
LIBRETRO_SLANG_SHADERS_LICENSE_FILES = COPYING

define LIBRETRO_SLANG_SHADERS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/recalbox/share_init/shaders/shaders_slang
	cp -a $(@D)/* \
			$(TARGET_DIR)/recalbox/share_init/shaders/shaders_slang
	rm -f $(TARGET_DIR)/recalbox/share_init/shaders/shaders_slang/Makefile \
		$(TARGET_DIR)/recalbox/share_init/shaders/shaders_slang/configure
endef

$(eval $(generic-package))
