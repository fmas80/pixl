################################################################################
#
# GLSL_SHADERS
#
################################################################################

LIBRETRO_GLSL_SHADERS_VERSION = 0893cfcf9131d9afe56e65a781e5136757eb7454
LIBRETRO_GLSL_SHADERS_SITE = $(call github,libretro,glsl-shaders,$(LIBRETRO_GLSL_SHADERS_VERSION))
LIBRETRO_GLSL_SHADERS_LICENSE = MIT
LIBRETRO_GLSL_SHADERS_LICENSE_FILES = COPYING

define LIBRETRO_GLSL_SHADERS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/recalbox/share_init/shaders/shaders_glsl
	cp -a $(@D)/* \
			$(TARGET_DIR)/recalbox/share_init/shaders/shaders_glsl
	rm -f $(TARGET_DIR)/recalbox/share_init/shaders/shaders_glsl/Makefile \
		$(TARGET_DIR)/recalbox/share_init/shaders/shaders_glsl/configure
endef

$(eval $(generic-package))
