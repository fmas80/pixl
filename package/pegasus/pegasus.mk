################################################################################
#
# PEGASUS
#
################################################################################

PEGASUS_SITE = https://github.com/pixl-os/pegasus-frontend
PEGASUS_VERSION = c386bb37e0dde980b62e0aa92276e9e6fb10dd1e
PEGASUS_LICENSE = GPLv3
PEGASUS_SITE_METHOD = git
PEGASUS_GIT_SUBMODULES = YES
PEGASUS_DEPENDENCIES = gstreamer1 qt5base qt5svg qt5declarative qt5multimedia \
						qt5graphicaleffects qt5tools qt5quickcontrols2 qt5quickcontrols \
						qt5virtualkeyboard qt5webengine qt5connectivity

PEGASUS_CONF_OPTS += "USE_SDL_GAMEPAD=1"

define PEGASUS_CONFIGURE_CMDS
	cp -R ../dl/pegasus/git/.git ../output/build/pegasus-$(PEGASUS_VERSION)/.git
	#run qmake
	$(QT5_QT_CONF_FIXUP)
	cd $(PEGASUS_BUILDDIR) && \
	$(TARGET_MAKE_ENV) $(PEGASUS_CONF_ENV) $(QT5_QMAKE) $(PEGASUS_CONF_OPTS)
endef

define PEGASUS_INSTALL_TARGET_CMDS
	$(INSTALL) -m 0755 -D $(@D)/src/app/pegasus-fe \
	$(TARGET_DIR)/usr/bin/pegasus-fe
endef

$(eval $(qmake-package))
