################################################################################
#
# xemu
#
################################################################################

# Commits on Sep 10, 2022
XEMU_VERSION = v0.7.70
XEMU_SITE = https://github.com/mborgerson/xemu.git
XEMU_SITE_METHOD=git
XEMU_GIT_SUBMODULES=YES
XEMU_LICENSE = GPLv2
XEMU_DEPENDENCIES = sdl2 host-python-pyyaml libgcrypt libpcap libglu

XEMU_EXTRA_DOWNLOADS = https://github.com/mborgerson/xemu-hdd-image/releases/download/1.0/xbox_hdd.qcow2.zip

## print version of core in PixL
define XEMU_PRE_CONFIGURE
	echo "xemu;xemu;${XEMU_VERSION}" > $(TARGET_DIR)/recalbox/share_init/system/configs/xemu.corenames
endef
XEMU_PRE_CONFIGURE_HOOKS += XEMU_PRE_CONFIGURE
XEMU_CONF_ENV += PATH="/x86_64/host/x86_64-buildroot-linux-gnu/sysroot/usr/bin:$$PATH"

# Compilations
# i386-softmmu
XEMU_CONF_OPTS += --target-list="x86_64-softmmu"
XEMU_CONF_OPTS += --cross-prefix="$(STAGING_DIR)"
XEMU_CONF_OPTS += --extra-cflags="-DXBOX=1 -O3 -Wno-error=redundant-decls -Wno-error=unused-but-set-variable"
# Targets and accelerators
# XEMU_CONF_OPTS += --enable-kvm
XEMU_CONF_OPTS += --enable-avx2
# XEMU_CONF_OPTS += --enable-hax

define XEMU_CONFIGURE_CMDS
	cd $(@D) && $(TARGET_CONFIGURE_OPTS) ./configure $(XEMU_CONF_OPTS)
endef

define XEMU_BUILD_CMDS
	$(TARGET_CONFIGURE_OPTS) CXX="$(TARGET_CXX) $(COMPILER_COMMONS_CXXFLAGS_NOLTO)" CC="$(TARGET_CC) $(COMPILER_COMMONS_CFLAGS_NOLTO)" \
		CC_FOR_BUILD="$(TARGET_CC) $(COMPILER_COMMONS_CFLAGS_NOLTO)" GCC_FOR_BUILD="$(TARGET_CC) $(COMPILER_COMMONS_CFLAGS_NOLTO)" \
		CXX_FOR_BUILD="$(TARGET_CXX) $(COMPILER_COMMONS_CXXFLAGS_NOLTO)" LD_FOR_BUILD="$(TARGET_LD) $(COMPILER_COMMONS_CXXFLAGS_NOLTO)" \
                CROSS_COMPILE="$(STAGING_DIR)/usr/bin/" \
                PREFIX="/x86_64/host/x86_64-buildroot-linux-gnu/sysroot/" \
                PKG_CONFIG="/x86_64/host/x86_64-buildroot-linux-gnu/sysroot/usr/bin/pkg-config" \
		$(MAKE) -C $(@D)
endef

define XEMU_INSTALL_TARGET_CMDS
	# Binaries
	cp $(@D)/build/qemu-system-x86_64 $(TARGET_DIR)/usr/bin/xemu

	# XEmu app data
	mkdir -p $(TARGET_DIR)/usr/share/xemu/data
	cp $(@D)/data/* $(TARGET_DIR)/usr/share/xemu/data/
	$(UNZIP) -ob $(XEMU_DL_DIR)/xbox_hdd.qcow2.zip xbox_hdd.qcow2 -d $(TARGET_DIR)/usr/share/xemu/data
endef

# Hotkeys using evmapy
define XEMU_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/xemu/xemu.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
XEMU_POST_INSTALL_TARGET_HOOKS = XEMU_EVMAP

$(eval $(autotools-package))
