################################################################################
#
# hid-nintendo
#
################################################################################

HID_NINTENDO_VERSION = 6f78c51cd3e4292976ee5304f9dedc316acf5a31
HID_NINTENDO_SITE = $(call github,nicman23,dkms-hid-nintendo,$(HID_NINTENDO_VERSION))
HID_NINTENDO_LICENSE = GPL-2.0
HID_NINTENDO_LICENSE_FILES = LICENSE
HID_NINTENDO_MODULE_SUBDIRS = src

$(eval $(kernel-module))
$(eval $(generic-package))
