################################################################################
#
# DOLPHIN_EMU
#
################################################################################

# https://fr.dolphin-emu.org/download/ for check versioning
# respect vx.x.xxxxx not x.x-xxxxx
DOLPHIN_EMU_VERSION_CORE = v5.0.17269
DOLPHIN_EMU_VERSION = 48c9c224cf9f82f0f9f2690b7cc6283d7448480c
DOLPHIN_EMU_SITE = https://github.com/dolphin-emu/dolphin
DOLPHIN_EMU_SITE_METHOD = git
DOLPHIN_EMU_LICENSE = GPL-2.0+
DOLPHIN_EMU_LICENSE_FILES = license.txt
DOLPHIN_EMU_DEPENDENCIES = libevdev ffmpeg zlib libpng lzo libusb libcurl bluez5_utils hidapi xz host-xz xserver_xorg-server qt5base
DOLPHIN_EMU_SUPPORTS_IN_SOURCE_BUILD = NO
DOLPHIN_EMU_GIT_SUBMODULES = YES

## print version of core in PixL
define DOLPHIN_EMU_PRE_CONFIGURE
	echo "Dolphin-emu;dolphin;${DOLPHIN_EMU_VERSION_CORE}" > $(TARGET_DIR)/recalbox/share_init/system/configs/dolphin.corenames
endef
DOLPHIN_EMU_PRE_CONFIGURE_HOOKS += DOLPHIN_EMU_PRE_CONFIGURE

DOLPHIN_EMU_CONF_OPTS += -DDISTRIBUTOR='pixL-os'
DOLPHIN_EMU_CONF_OPTS += -DUSE_MGBA=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_LTO=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_EGL=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_NOGUI=OFF
DOLPHIN_EMU_CONF_OPTS += -DUSE_DISCORD_PRESENCE=OFF
DOLPHIN_EMU_CONF_OPTS += -DTHREADS_PTHREAD_ARG=OFF
DOLPHIN_EMU_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_AUTOUPDATE=OFF

# Hotkeys using evmapy
define DOLPHIN_EMU_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/dolphin-emu/dolphin.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
DOLPHIN_EMU_POST_INSTALL_TARGET_HOOKS = DOLPHIN_EMU_EVMAP

$(eval $(cmake-package))
