# Change Log
All notable changes to this project will be documented in this file (focus on change done on RETRO-X branch).

## [pixL] - Betaxx - xxxx-xx-xx
- OS:
	- Save online update FbNeo resources in libretro-fbneo package
	- Bump Pcsx2 and save online update Pcsx2 resources
	- Bump Xemu to v0.7.70 and add online update-resources
	- move repository to github for supermodel and bump to r895
	- Bump dolphin-emu to 5.0-17269
	- Bump Duckstation new latest version 2022-05-19
	- Fix force disabled Xemu cache disk option 
	- Change Mame default emulator to libretro-mame
	- Change Nintendo 64 default emulator to libretro-parallel_n64 #[20]
	- Fix error when save/change favorites #[19]
	- Add wheel mapping :
		- Microsoft SideWinder Precision Racing Wheel USB version 1.0
	- Fix vulkan for librerto-pcsx2 #[15]
	- Fix N64 mupen64plus glide64mk2 no FPS print #[2]

## [pixL] - Beta28 - 2022-09-12
- OS:
	- Add support for vulkan (only for nvidia-driver)
	- Add vulkan shaders and update opengl shaders
	- Fix bios xml cleaning
	- Move arcade dat generator to pixl-os repos

- Emulation:
	- Add missing evmapy for citra and supermodel
	- Bump libretro-flycast to v2.0
	- Add evmapy file for retroarch to be able to manage multi-windows using Hotkey+R1
	- Bump libretro-mame to v0.247

- Pegasus-Frontend:
	- Add usersettings folder in recalbox/share/saves/ for preserve favorite game file on first
	- New API features ('ScreenScraperId','type', 'releasedate' & 'manufacturer')
	- Theme translation feature support
	- Multi-windows support
	- GameOS:
		- new design management : include a designer for main view + vertical List feature
		- Multi-languages-support : introduce translation capacity in theme using linquist tools
		- select random game in games list with R1+L1 (from Grid or VerticalList)
	- rework video settings
	- see https://github.com/pixl-os/pegasus-frontend/blob/pixL-master/CHANGELOG.md for other features/fixes

## [pixL] - Beta27 - 2022-07-09
- OS:
	- Change size-partition boot 6Gb to 9Gb
	- Change size-partition overlay 1Gb to 2Gb
	- Add 12 images pixl when the install OS
	- Add 10 images pixl when the update OS
	- solved issues textes in pictures
	- Add video create partition pixL
	- Add 10 videos intro pixL
	- delete all intro video recalbox
	- Add 5 gamepad mapping USB
		- Sony Interactive Entertainment Wireless Controller USB
		- Generic X-Box pad : Microsoft X-Box One Series pad USB
		- Microsoft X-Box One pad (Firmware 2015) USB
		- Nacon PS4 Revolution Pro Controller 2 USB
		- PS5 Official Weifang Goertek Electronics Co.Ltd wireless
	- Add 2 wheel mapping
		- Thrustmaster F430 Cockpit Wireless
		- ThrustMaster Ferrari 458 Racing Wheel
	- Add Mesa3d Drivers :
		- GALLIUM_DRIVER_VC4
		- GALLIUM_DRIVER_V3D
		- GALLIUM_DRIVER_R600
		- GALLIUM_DRIVER_R300
		- OSMESA_GALLIUM
		- VDPAU
		- INTEL_MEDIADRIVER
	- Add lmsensors for better temp informations
	- Add walppaper in openbox with feh package
	- Add Naomi2 Arcade system thanks Akkeoss
	- Adding evmapy tools to map pad to keyboard for emulators hotkeys
	- Adding evdev python library to use with evmapy
	- Populate share from share_init for evmapy also
	- Add notify-tools for evmapy usage
	- Add python setuptools also for evmapy usage
	- Integration xdotool to manage simulation of key/mouse
	- fix configgen-recalbox package to build python-evdev also
	- fix build for python-evdev package
	- fix build for evmapy package
	- fix on nvidia version/install scripts to ignore igpu if it's a Desktop PC. (no considered as optimus case/conflict)
	- fix PATCH change to BlendFunc in flycast for crosshair
	- Change password root

- Emulation:
	- Dolphin-triforce standalone emulator:
		- Add triforce core details in .corenames files
		- Add evmapy keys in build for dolphin-triforce emulator
		- New mapping dolphin-triforce to have menu in full screen
		- Fix evmapy mappings for dolphin-triforce: remove "sequence" mode not applicable for alt+tab
		- Add hotkey+r1 in triforce .keys evmapy file to switch between emulator and pegasus using multi-windows feature
		- fix to improve switching between game/pegasus in case of multi-windows from evmapy
		- fix to improve also pause/unpause and screenshot behaviors for evmapy .keys file for dolphin-triforce
	
	- Dolphin-emu standalone emulator:
		- fix to add missing extension gcz for wii system
		- Add evmapy keys in build for dolphin-emu emulator (impacts for wii & gamecube systems)
		- Add a click of mouse in evmapy after alt+tab to keep focus for dolphin
		- Fix evmapy mappings for dolphin (wii & gamecube): remove "sequence" mode not applicable for alt+tab
		- Usage of a unique evmapy 'dolphin.keys' file to use a new management of multi-windows and menu/pegasus swicth
		- Fix evmapy .keys file for dolphin to detect game window
		- Add sideways toogle for emulated wiimote on dolphin using hotkey+L2
		- fix to improve switching between game/pegasus in case of multi-windows from evmapy
	
	- Xemu standalone emulator:
		- Add patch for xemu to display menu using Hotkey+A (xbox buttons mapping)
		- Fix on xemu build to use libpcap/libglu when we build emulator independently
		- Workaround to manage SLD2 custom mappings in xemu from Pegasus-Frontend ones
		- add eeprom files for 7 languages in configs of xemu for XBOX
		- add evmapy .keys file for xemu and in .mk file
		- update boot script to copy eeprom files from share_init for system/configs/xemu
	
	- Pcsx2 standalone emulator:
		- fix to remove deprecated hotkey patch (replaced by evmapy usage)
		- fix mappings using pegasus-frontend sdl controllers file (in one line ;-)
		- fix to use a new way to manage better pad order using env variable
		- add evmapy .key file for PCSX2 to have all hotkeys ready
	
	- Bump Configgen to have changes from configgen for evmapy and last improvements:
		- for dolphin-emu:
			- best language management (using update of SYSCONF)
			- improve hotkey management/pause/switching
			- fix buttons position on gamecube for Z/L/R
			- fix buttons mapping for B/2 buttons on wii
			- manage hotkey for wiimote direction to help gameplay with wiimote in games
			- fix for gamecube to replicate L/R to L-Analog & R-Analog
		- for dolphin-triforce:
			- controllers mapping
			- best hotkey+b management
			- force widescreen hack on ratio to 16/9
			- add load save state automatically if needed using xdotool
			- add cleaning of "card"
			- remove auto replacements of controls using auto-detection of axis/buttons
		- for pcsx2 
			- new pad order management and remove usage of PAD.ini
		- for xemu: 
			- move initial hdd file only for selected and used system
			- add languages management for xbox

- Pegasus-Frontend:
	- Bump of version 0.0.7:
		- fix on management of shaders display/selection
		- i915 Driver force-probe activation integration (to correct some issues with sound outputs)
		- fix refresh functions for wifi, bluetooth and settings view (due to bad timeout parameter)
		- add fr translation for i915 force-probe menu + fixes (wifi notes)
		- GameOS-pixL:
			- adding logos for new 'naomi2' system and also new 5 logos for screenshots 'system' and also ports system

## [RETRO-X] - Beta 26 - 2022-06-03
- OS:
	- Change grub name Recalbox by PixL
	- Remove video recalbox for introduction
	- Add video pixl for introduction
	- Remove image recalbox when the install OS
	- Add image pixl when the install OS
	- Fix boot script for nvidia-install to be executed in all cases (with or without execution right on file)
	- Bump last 'hardware' pixL branch
	- Fix nvidia install execution to use bash and not only execution right
	- Disable Asrock led controller because is not a USB joystick
 - Lightgun:
	- Recovering of original lightgun feature using lightgun.cfg initial format
	- Take into account last games introduction done previously, version 1.0.5 of file restored
	- Compatible with libretro lightgun configgen 1.6.7
	- New bump of configgen necessary


- Emulation:
	- Fix libretro-swanstation build and bump to lastest version
	- Bump librerto-mame on 0243 mame romset
	- Bump pcsx2 to v1.7.2744
	- Bump citra-emu to nightly-1764
	- Bump Hypseus to v2.8.2a
	- Bump Supermodel to 882 
	- Introduction of new system for microsoft xbox
	- Introduction of new system for sega chihiro (alpha version)
	- Introduction of new system for triforce (alpha version)
	- Bump of corresponding configgen

- Pegasus-Frontend:
	- New bump of version 0.0.6:
		- including new feature to manage Wifi (with fr translation + issue 15 corrected)
		- Fix on controllers naming
		- New GameOS-pixL theme:
			- including new system logos
			- fix on odyssey 2 logos
			- Use L1/R1 for letter nav & L2/R2 for system nav in platform page
			- fix on gridview width in platform page
			- Introduction of overlays & logos options in gameView
			- adding "beta" logo on system using an emulator 'low'

## [RETRO-X] - Beta 25 - 2022-05-06
- OS:
	- Improvements on Nvidia drivers:
		- Use a unique local repo for nvidia driver in the OS (to avoid to propose several version of drivers)
		- fix bug on install of version 510.XX.XX
		- fix to compare versions installed and proposed to let capacity to install a more recent version.
- Pegasus-Frontend:
	- New bump of version 0.0.5:
		- including new feature to manage multiversion as for Nvidia drivers
		- fix to manage index of version depending of compatibility and version scripts
		- fix to delete some version.sh/install.sh from /tmp to avoid mismatch or corrupted files

## [RETRO-X] - Beta 24 - 2022-05-05
- OS:
	- Improvements on Nvidia drivers:
		- Add new way to install Nvidia drivers as any update proposed by pegasus
		- including install/version scripts and corresponding json files for nvidia local updates

- Pegasus-Frontend:
	- Bump of version 0.0.5:
		- new feature including nvidia drivers installation as udpate proposed by Pegasus (need reboot)
		- improvement and fix to well manage updates remotly but also local.
		- fix to clarify the progress bar animation and using colors on results (green for ok, red for not ok)
		- fix on lang: update for controller helps/views [Thanks Sebio]

## [RETRO-X] - Beta 23 - 2022-05-01
- OS:
	- Improvements on Nvidia drivers:
		- Manage 3 versions now: 390/460 & 510
		- New files for compatibility with all M devices (especially for 390 & 460 driver versions)
		- Improve check of reference using compatibility files including "tab" characters to well detect GPUID
		- change of Nvidia-install to manage nvidia optimus cases (using nvidia + intel GPUs)
		- Change recalbox-boot.conf to activate nvidia optimus case (nvidia-optimus=true)

- Emulation:
	- fix .corenames copy to do from share_init for standalone cores

- Pegasus-Frontend:
	- Bump of version 0.0.4:
		- lang: bump last translation fr (update on bios, restart and controller menu)
		- introduction of online update for cores
		- first updatable core: libretro fbneo

## [RETRO-X] - Beta 22 - 2022-04-22
- OS:

	- Add feature to activate installation or not from recalbox-boot.conf for nvidia drivers
	- Change recalbox-hardware source before to use the mono-repo
	- New nvidia-install to manage 'nvidia-driver=true' using new recalbox-hardware
	- hid-nintendo: update mapping for snes, megadrive/genesis & n64 to improve buttons usage
	- Set default settings in recalbox.conf for bluetooth, pegasus, overlays, controllers and default mame core for x86 64 bits

- Emulation:

	- remove ps4 legacy mapping
	- add/fix xbox 360 wireless and corrected ps4 mappings
	- add Nintendo Switch snes/genesis/megadrive pads mappings
	- add Nintendo Switch nes pad mapping
	- add Nintendo Switch N64 pad mapping
	- update Nintendo Switch joycon L & R mappings adapted (need joycond)
	- update Snakebyte idroid:con to have L1/R1
	- add NEOGEO mini PAD mapping
	- Bump Configgen to remove fix on Left stick for Flycast

- Pegasus-Frontend:

	- Bump pegasus including:
		- Refactoring of menu using icons in sections
		- Refactoring of menu using hide/display using button on sections

		- Bluetooth improvements:
			- fix asynchronous disconnection using bluetooth
			- fix to remove not well paired device or not identified as paired.
			- fix to add devices already paired in recalbox.conf
			- fix to improve verification/timing for devices already paired
			- Introduce parameter and menu to be able to reset bluetooth at each start of Pegasus
			
		- Controller improvements
			- change way to find mapping (es_input -> sdl2)
			- set way to manage order  of player/device from menu
			- add icon for 8BitDo SN30pro+
			- icon: to add arcade sticks (8 buttons) black & white
			- add icon for Snakebyte idroid:con
			- game controller database: updated to accept SDL 2.0.16 format from SDL 2.0.14 
			- game controller database: add last version from https://github.com/gabomdq/SDL_GameControllerDB
			- add matching for drangonrise and xinmo using arcade panel icon
			- sdl2: fix for build using SDL_JoystickDevicePathById
			- udev: add code to integrate indexation from udev
			- icons: add detection of ultimarc j-pac as arcade panel device
			- icons: use icon of sn30 pro+ for pro 2 from 8bitdo
			- icons: to well distinguish 8bitdo sn30 pro+/pro plus & pro 2

			- custom layout introduction:
				- nes & snes controllers layout
				- xbox360 controller layout
				- ps4 controller layout

			- add xow daemon management for xbox one/series controllers
			- layout: force to default layout if doesn't exist for any controller

		- sysinfo:
			- more info added as temp and gpu in 2 columns
			- fix to have best temperature display to ignore some cases and doubloon

		- lang : add last fr translation
		- confirm in title/comments that Sony part is only for PS3 sixasis
		- bluetoohth: fix to restart bluetooth only during starting and not reloading after game session
		- accountmain: add fix on SectionTitle to set font indendently
		- controllersmain: reduce controller icon of 10% and centralize verticaly

## [RETRO-X] - Beta 21 - 2022-03-11
- OS:
	- feature : hid-nintendo driver patches to add nes, snes, n64 and genesis (and avoid crashes)
	- fix: hid-nintendo driver to disable imu devices

- Tools:
    - arcade dat generator introduction
	
- Emulators:
	- bump supermodel: bump and fix dump texture option
	- fix for citra emu configs
	- bump libretro mame: 0.239
	
## [RETRO-X] - Beta 20 - 2022-02-13
- OS:
	- Add QT5XmlPattern library for bios checking
	- Add QT library for usage of controller with virtual keyboard

- Emulators:
	- add version for all standalone emulators during build in dedicated *.corenames files
	- Supermodel bump to 880 : add dump of textures

- Pegasus:
	- introduction of "information system" view to display cpu, memory, mass storage, etc...
	- introduction of "virtual keyboard" component
	- introduction of "updates" management feature
	- introduction of "Bios Checking menu" using API with md5 calculation for bios
	- Significant other things:
		- restart feature added in menu with popup of confirmation
		- add display of version for all standalone emulator also as for libretro ones
		- improvements on loading performance (add skraper media option / change collections management in post-processing)
		- fix support of "hidden" tag from gamelists
		- versionning: add version from git release and OS version in Pegasus
		- localization: fix "american english" multiple menu display in buildroot
	
- Theme (gameOS-pixL):
	- demo mode: remove fading/lists and highlight in demo mode
	- select search directly from y/triangle button directly and not only filter
	- setup of virtualkeyboard for search in theme
	- introduce demo resetting at OnRelease + fix on settings
	- add virtual keyboard support in settings of theme
	- fix in settings to use good variable type
	- fix gridviewmenu to avoid to launch game from empty list and to gameview
	- add Change & Edit word in help for settings
	- fix showcaseLoader to load in all cases at start and after game ending
	- fix for white/color logo in gameview

## [RETRO-X] - Beta 19 - 2021-12-31
- Emulators:
	- Introduction of Citra emulator for 3DS and adapted to new configgen using Python 3.9 (still issue on language selection)
	- fix citra and ps2 romfs2
	- bump standalone supermodel: r874
	- bump standalone dolphin-emu: 5.0-15445
	- bump libretro mame: 0.237
	- bump pcsx2(+ patch reduced) : v1.7.2148
	- fix flycast: to forced left analog on dpad mode for Atomiswave, Naomi and NaomiGD

- Pegasus:
	- fix in controllers naming
	- translation updated for French and English
	- fix on bluetooth to support legacy scripts now (since rebase recalbox 8.0)
	- menu: overlay options / icons added in menu/section
	- cd-rom management:
		- introduction using new dialog box / assets 
		- new API principle: the single play 
		- but still under progress (not yet fully available for users)
	- Netplay feature introduction:
		- Include lobby with medida display / icons / logo
		- Friend's room management
		- options of filtering (friend, playable games, etc..)
		- Enhanced check before to launch game to avoid "black screen effect"

- Theme (gameOS-pixL):
	- introduction of Netplay in helpbar & Gameview
	- gameView: to use L1/R1 to change game in a system
	- introduction of demo mode in the theme itself using gameView
	- fix L2/R2 letter scrolling in system view

- OS:
	- rebase on master of recalbox 8.0 electron
	- add new externalscreen for 2 display layout
	- project-cd:
		- add launch_cdrom and udev rules 
		- add eject fonction and change udev rules

## [RETRO-X] - Beta 18 - 2021-10-13
- Pegasus :
	- Change version to "PEGASUS (pixL version)"
	- Improve Pegasus loading bar using more information and steps (including fixes now)
	- Display all controllers connected in menu Controllers
	- Fixes on GamepadEditor for new controllers
	- New bluetooth feature for controllers and more (as bluetooth speaker and headset):
		- Several methods of scanning/pairing and to forget devices
		- Show vendor name if available
		- Hide no name devices
		- Visibility of paired devices
		- Possibility to ignore devices
		- Forget and disconnect feature independently by device
		- Icon avaialble for several devices		
		- Show device status as paired/no success pairing and connected
		- Battery display (sony/nintendo devices)
	
- Theme (GameOS-pixL):
	- add play time, play count and last played in game info and also in settings
	- add new feature to change automatically favorites displayed in header of showcaseview	

- OS:
	- Add QT5 Connectivity dependencies for bluetooth features

## [RETRO-X] - Beta 17 - 2021-09-09
- fix SQLITE Drivers for QT (for Pegasus DB)
- Pegasus : 
	- fix to correct issue with "unknown" controllers by SDL
- GameOS theme:
	- performance and visibility optimization especially for main menu
	- Collections management (settings and display in 'main menu' collections for the moment)
	- arcade button in helps
	- gameinfo improvements
	
## [RETRO-X] - Beta 16 - 2021-08-28
- fix citra and pcsx2 not launch migration python3.9
- fix romfs v2 ps2
- bump configgen for fix citra and pcsx2
- bump configgen for fix citra and pcsx2
- bump pegasus for lastest fix/improvements:
	- fixes: fonts / helps / keyboard menu / logo selection in settings menu / binding improved / warning removed
	- Performance improvement on main menu

## [RETRO-X] - Beta 15 - 2021-08-09
- add ps2 and 3ds romfs v2 (bump configgen)
- "double" boot partition on pc x86-64 to prepare the future
- Pegasus:
	- popup for controllers connection including icons (ps/xbox & wheels)
	- new controller experience proposing to configure/test during first connection
	- several improvements in gamepad editor (colors, buttons, help)
	- Warning: 'step by step' not yet available but help instruction are displayed ;-)
	- New systemList.xml from romfs v2 supported
	- Fixes for issues on gameOS theme for default value from settings (as language and logos) - (#11,#13)
	- new API from Pegasus to manage list of parameters listed from system command.

## [RETRO-X] - Beta 14 - 2021-07-07
- set mame on default arcade emulator (#12)
- add pdfjs component in scripts directory as for gameos-pixl
- Pegasus:
		- fix keyboard qwerty retroachievement menu (and netplay) (#9)
		- retroachievements feature (including new gameos with new gameview retroachievements display)
		- logo optimization
		- pdfjs library from OS
		- rework menu and several fixes
		- link between languages of pegasus and systems
		- keyboard region management and change in real-time
		- add shaders selection in menu

## [RETRO-X] - Beta 13 - 2021-06-19
- Fix Ci gitlab (#6)
- Fix lags/skips during cinematics or in game mode 3DS citra-emu (#3)
- Fix fullscreen is never kept on 3DS Citra-emu (#5)
- Bump configgen :
    - fix L and R not work
- add gstreamer full plugins :
    - fix mute video preview in gameOs
    - fix many video preview not work

## [RETRO-X] - Beta 12 - 2021-05-25
- add nintendo 3ds citra emu:
    - add citra on verion nightly-1311
    - add recalbox romf package
    - change to citra qt and change repository
    - bump citra to nightly 1661
    - fix compilation mistake on defconfigs
    - bump configgen and citra emu
- bump last version of Pegasus (commit: 42c2... ):
    - including new translation in french for new menu
    - mouse fix in menu (with possibility to deactivate or not)
- bump last version of Configgen (commit: 609a...) for ratio improvement
- supermodel: remove option for video engine and fix ratio option / save states fix

## [RETRO-X] - Beta 11 - 2021-05-23
- add THEMES to manage themes directory from share
- bump last version of Pegasus (commit: 6f0d...) / come back to use commit and not branch name to avoid problem of build:
    - with best performance (Gamelist Only)
    - add hash management for theme as GameOS usage
- test version for Supermodel (new compilation/configgen)

## [change-frontend-to-pegasus] - Beta 10 "dirty" - 2021-04-29
- including PS2 test version

## [change-frontend-to-pegasus] - Beta 10 - 2021-04-22
- pegasus version with storage selection as "NETWORK, INTERNAL, etc.." and with sound management
    
## [change-frontend-to-pegasus] - Beta 9 - 2021-04-16
- new pegasus version:  
    - with new manual management and more media for our GameOS Theme fork ;-)
    - management of keyboard simulation with pad for browser browsing used in pdf manual display.
- based on recalbox 7.2 Beta 23 (with Mame 0.230... and other things)
- nvidia 460 drivers

## [change-frontend-to-pegasus] - Beta 8 - 2021-04-10
- add QT5 WEB ENGINE for future used ;-)

## [change-frontend-to-pegasus] - Beta 7 - 2021-04-02
- fix on S14migrate boot script: remove migration part about virtual keyboard
- re-add qt5 quick control in defconfig + virtual keyboard in .in/.mk

## [change-frontend-to-pegasus] - Beta 6 - 2021-04-01 (including change done for beta 1 to 5)
- fix(intel): disable VSYNC using "vblank_mode=0" to launch "pegasus-fe"
- kill pegasus if "es stop" command launched 
- add gstreamer for qt5multimedia
- add reboot and shutdown for buildroot (patch on pegasus)
- fix sound and video launch for pegasus
- change repository for pegasus (to use our fork finally)
- remove reboot and shutdown patch of pegasus
- add QT virtualkeyboard
- add qt5base_xml for xml and qt5quickcontrols for next popups
- use recalbox-integration branch for pegasus
- add qt5 quick controls 2 
- re-add qt5 quick control in defconfig + virtual keyboard in .in/.mk

## [change-frontend-to-pegasus] - 2021-02-03
- New branch created to integrate the new front-end for recalbox - Pegasus Front-end
- init package pegasus frontend
